function geraNumero() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const randomNumber = Math.floor(Math.random() * 10) + 1;
      if (randomNumber < 5) {
        resolve(randomNumber);
      } else {
        reject(new Error("Número maior ou igual a 5."));
      }
    }, 3000);
  });
}


geraNumero()
  .then((number) => {
    console.log("Número aleatório menor que 5:", number);
  })
  .catch((error) => {
    console.error("Erro:", error.message);
  });
